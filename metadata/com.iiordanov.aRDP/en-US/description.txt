Some people have complained of issues after a major upgrade of the FreeRDP library in v3.8.7. I've made sure to upload older versions of aRDP (APK files) going back to at least v3.6.5 here:
https://github.com/iiordanov/remote-desktop-clients/releases

If aRDP doesn't work for you, before writing a review, please post your question in the forum:
https://groups.google.com/forum/#!forum/bvnc-ardp-aspice-opaque-remote-desktop-clients

See below for setup instructions on enabling RDP on Windows.

Current known issues:
- Does not work for accounts with no password.
- Does not work for users with cyrillic letters in the user name.

aRDP is a secure, SSH capable, open source Remote Desktop Protocol client that uses the excellent FreeRDP library and parts of aFreeRDP. Its features include:

- Remote desktop control of computers running any version of Windows
- Remote desktop control of Linux computers with xrdp installed.
- Master password
- Multi-factor (two-factor) SSH authentication
- Sound redirection
- SDcard redirection
- Console mode
- Fine control over remote desktop session styling
- Multi-touch control over the remote mouse. One finger tap left-clicks, two-finger tap right-clicks, and three-finger tap middle-clicks
- Right and middle-dragging if you don't lift the first finger that tapped
- Scrolling with a two-finger drag
- Pinch-zooming
- Force Landscape, Immersive Mode, Keep Screen Awake options in Main Menu
- Dynamic resolution changes, allowing you to reconfigure your desktop while connected, and control over virtual machines from BIOS to OS
- Full rotation support. Use the central lock rotation on your device to disable rotation
- Multi-language support
- Full mouse support on Android 4.0+
- Full desktop visibility even with soft keyboard extended
- SSH tunneling for added security or to reach machines behind a firewall.
- UI Optimizations for different screen sizes (for tablets and smartphones)
- Samsung multi-window support
- SSH public/private (pubkey) support
- Importing encrypted/unencrypted RSA keys in PEM format, unencrypted DSA keys in PKCS#8 format
- Automatic connection session saving
- Zoomable, Fit to Screen, and One to One scaling modes
- Two Direct, one Simulated Touchpad, and one Single-handed input modes
- Long-tap to get a choice of clicks, drag modes, scroll, and zoom in single-handed input mode
- Stowable on-screen Ctrl/Alt/Tab/Super and arrow keys
- Sending ESC key using the "Back" button of your device
- Ability to use D-pad for arrows, and to rotate D-pad for some bluetooth keyboards
- Minimum zoom fits screen, and snaps to 1:1 while zooming
- FlexT9 and hardware keyboard support
- Available on-device help on creating a new connection in the Menu when setting up connections
- Available on-device help on available input modes in the Menu when connected
- Tested with Hackerskeyboard. Using it is recommended (get hackers keyboard from Google Play).
- Export/Import of settings

Planned features:
- Clipboard integration for copy/pasting from your device
- Choosing a different color depth
- Audio support

Instructions on how to enable Remote Desktop on Windows:
http://www.howtogeek.com/howto/windows-vista/turn-on-remote-desktop-in-windows-vista/

aRDP is the sister project of bVNC and they share a common code-base. GPL source code here:
https://github.com/iiordanov/remote-desktop-clients